import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { EditServiceService } from '../edit-service.service';
import { SynonymService } from '../synonym.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class ControlPanelComponent implements OnInit {
  isBoldSelected = false;
  isItalicSelected = false;
  isUnderlineSelected = false;
  selectedWord = '';
  synonymus;
  selectedSynonym;

  constructor(
    private editServiceService: EditServiceService,
    private synonymService: SynonymService) {
  }

  ngOnInit() {
    // Get styles from file components, get synonyms from service
    this.editServiceService.getDataForControlPannel()
      .subscribe(message => {
        this.isBoldSelected = false;
        this.isItalicSelected = false;
        this.isUnderlineSelected = false;

        if (message.target) {
          this.selectedWord = message.target;
          // Taking synonyms from service
          this.synonymService.getSynonym(message.target)
            .subscribe(resp => this.synonymus = resp); // Yes, inner subscribe is bad practice
        } else {
          this.selectedWord = '';
          this.selectedSynonym = '';
          this.synonymus = null;
        }

        // Applying styles to buttons depends on selected word styles
        if (message.mess) {
          if (message.mess.classList.contains('bold')) {
            this.isBoldSelected = true;
          }
          if (message.mess.classList.contains('italic')) {
            this.isItalicSelected = true;
          }
          if (message.mess.classList.contains('underline')) {
            this.isUnderlineSelected = true;
          }
        } else {
          this.isBoldSelected = false;
          this.isItalicSelected = false;
          this.isUnderlineSelected = false;
        }
      });
  }

  // Pass bold style to selected word, toggle bold button
  toggleBold() {
    if (!this.selectedWord.length) {
      return;
    }
    this.editServiceService.sendMessage('bold');
    this.isBoldSelected = !this.isBoldSelected;
  }

  // Pass italic style to selected word, toggle italic button
  toggleItalic() {
    if (!this.selectedWord.length) {
      return;
    }
    this.editServiceService.sendMessage('italic');
    this.isItalicSelected = !this.isItalicSelected;
  }

  // Pass underline style to selected word, toggle underline button
  toggleUnderline() {
    if (!this.selectedWord.length) {
      return;
    }
    this.editServiceService.sendMessage('underline');
    this.isUnderlineSelected = !this.isUnderlineSelected;
  }

  onChange(event) {
    this.selectedSynonym = event.target.value;
  }

  applySynonym() {
    // Pass synonyms to file component
    if (this.synonymus) {
      this.editServiceService.sendMessage(this.selectedSynonym || this.synonymus[0].word);
      this.selectedWord = this.selectedSynonym || this.synonymus[0].word;
    }
    this.selectedSynonym = '';
  }
}
