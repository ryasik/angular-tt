import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SynonymService {

  constructor(private http: HttpClient) { }

  // Geting synonyms from Datamuse API
  getSynonym(searchWord) {
    const url = 'https://api.datamuse.com/words?ml=' + searchWord;
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
      })
    };
    return this.http.get(url, httpOptions);
  }
}
