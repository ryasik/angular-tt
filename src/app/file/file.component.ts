import { ChangeDetectionStrategy, Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { TextService } from '../text-service/text.service';
import { Renderer, Renderer2 } from '@angular/core';
import { EditServiceService } from '../edit-service.service';
import { Subscription } from 'rxjs';
// import { ConsoleReporter } from 'jasmine';


@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileComponent implements OnInit {
  text$: Promise<void>;
  rendertext: String;
  modifText: string;
  targetElement = null;
  subscription$: Subscription;
  @ViewChild('divMessages', { read: ElementRef }) private divMessages: ElementRef;

  constructor(
    private textService: TextService,
    private renderer: Renderer2,
    private render: Renderer,
    private editServiceService: EditServiceService) {
  }

  ngOnInit() {
    // Receiving selected style from control-panel / synonym for selected word
    this.subscription$ = this.editServiceService.getMessage().subscribe(message => {
      if (message.text === 'bold') {
        this.addBold();
      } else if (message.text === 'italic') {
        this.addItalic();
      } else if (message.text === 'underline') {
        this.addUnderline();
      } else {
        this.replaceText(message.text);
      }
    });


    // Receiving text, spliting text by word, wrapping each word into span tag
    this.text$ = this.textService.getMockText().then(res => {
      const texts = res.trim().split(/\b(?!\s)/);
      for (let i = 0; i < texts.length; i++) {
        const span = this.renderer.createElement('span');
        const text = this.renderer.createText(texts[i]);
        this.renderer.appendChild(span, text);
        this.renderer.appendChild(this.divMessages.nativeElement, span);
      }
    });
  }

  // Actions on selecting each word
  selectWord(event) {
    if (event.target.nodeName === 'SPAN') {
      this.targetElement = event.target;
      this.removeSelected();
      this.renderer.addClass(this.targetElement, 'selected'); // Adding 'selected' class to selected word
      this.editServiceService.sendWord(event.target, event.target.innerHTML);
    } else {
      this.editServiceService.sendWord(null, null);
      this.targetElement = null;
      this.removeSelected();
    }
  }

  // Applying bold style for selected word
  addBold() {
    if (this.targetElement) {
      if (this.targetElement.classList.contains('bold')) {
        this.renderer.removeClass(this.targetElement, 'bold');
      } else {
        this.renderer.addClass(this.targetElement, 'bold');
      }
    }
  }

  // Applying italic style for selected word
  addItalic() {
    if (this.targetElement) {
      if (this.targetElement.classList.contains('italic')) {
        this.renderer.removeClass(this.targetElement, 'italic');
      } else {
        this.renderer.addClass(this.targetElement, 'italic');
      }
    }
  }

  // Applying underline style for selected word
  addUnderline() {
    if (this.targetElement) {
      if (this.targetElement.classList.contains('underline')) {
        this.renderer.removeClass(this.targetElement, 'underline');
      } else {
        this.renderer.addClass(this.targetElement, 'underline');
      }
    }
  }

  // Replacing of selected word by synonym word
  replaceText(word) {
    const elements = this.divMessages.nativeElement.children;
    for (let i = 0; i < elements.length; i++) {
      if (elements[i].classList.contains('selected')) {
        if (elements[i].innerHTML[0] === elements[i].innerHTML[0].toUpperCase()) {
          const fitstLetter = word[0].toUpperCase();
          const restWord = word.slice(1);
          elements[i].innerHTML = fitstLetter + restWord + ' ';
        } else {
          elements[i].innerHTML = word + ' ';
        }
      }
    }
  }

  // Removing 'selected' class from all elements
  removeSelected() {
    const elements = this.divMessages.nativeElement.children;
    for (let i = 0; i < elements.length; i++) {
      if (elements[i].classList.contains('selected')) {
        this.renderer.removeClass(elements[i], 'selected');
      }
    }
  }
}
