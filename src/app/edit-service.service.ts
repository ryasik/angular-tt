import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EditServiceService {

  constructor() { }

  private subject = new Subject<any>();
  private subject2 = new Subject<any>();

  sendMessage(message: string): void {
    this.subject.next({ text: message });
  }

  sendWord(mess: string, target: string): void {
    this.subject2.next({ mess, target });
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  getDataForControlPannel(): Observable<any> {
    return this.subject2.asObservable();
  }
}
